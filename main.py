class Moradia:
    def __init__(self, area, endereco, numero, preco_imovel, tipo = 'Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo

    def gerar_relatorio_preco(self):
        return int(self.preco_imovel / self.area)
    
class Apartamento(Moradia):
    def __init__(self, area, endereco, numero, preco_imovel,preco_condominio,num_apt,num_elevadores, tipo="Apartamento"):
        super().__init__(area, endereco, numero, preco_imovel, tipo=tipo)
        self.preco_condominio = preco_condominio
        self.andar = int(num_apt / 10)
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
    
    def gerar_relatorio(self):
        return "{} - apt {} - andar: {} - elevadores: {} - preco por m²: R$ {}".format(self.endereco,self.num_apt,self.andar,self.num_elevadores,self.gerar_relatorio_preco())